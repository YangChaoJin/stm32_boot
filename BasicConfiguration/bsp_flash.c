/*
*********************************************************************************************************
*	                                  
*	文件名称 : bsp_flash.c
*	版    本 : V1.0
*	说    明 : flash编程驱动
*	修改记录 :

*********************************************************************************************************
*/
#include "bsp_flash.h"

#define STM32_FLASH_BASE 0x08000000 	//STM32 FLASH的起始地址

#define STM32_FLASH_SIZE 128  //定义flash的大小

u16 gIAPBUF[1024];

/*
*********************************************************************************************************
*	函 数 名: FLASH_ReadHalfWord
*	形    参：addr : 要读数据的地址
*			  
*	返 回 值: 该地址的数值
*********************************************************************************************************
*/
u16 FLASH_ReadHalfWord(u32 addr)
{
	return *(vu16*)addr; 
}
/*
*********************************************************************************************************
*	函 数 名: FLASH_Write_NoCheck(u32 WriteAddr,u16 *pBuffer,u16 NumToWrite)   
*	形    参：WriteAddr : 要写数据的地址
*			  		pBuffer:pBuffer:数据指针
*						NumToWrite:半字节数
*	返 回 值: 无
*********************************************************************************************************
*/
void FLASH_Write_NoCheck(u32 WriteAddr,u16 *pBuffer,u16 NumToWrite)   
{ 			 		 
	u16 i;
	for(i=0;i<NumToWrite;i++)
	{
		FLASH_ProgramHalfWord(WriteAddr,pBuffer[i]);
	  WriteAddr+=2;//地址增加2.
	}  
} 
/*
*********************************************************************************************************
*	函 数 名: FLASH_Read(u32 ReadAddr,u16 *pBuffer,u16 NumToRead)     
*	形    参：ReadAddr : 要读数据的地址
*						NumToRead:半字节数
*	返 回 值: 无
*********************************************************************************************************
*/
void FLASH_Read(u32 ReadAddr,u16 *pBuffer,u16 NumToRead)   	
{
	u16 i;
	for(i=0;i<NumToRead;i++)
	{
		pBuffer[i]=FLASH_ReadHalfWord(ReadAddr);//读取2个字节.
		ReadAddr+=2;//偏移2个字节.	
	}
}
/*
*********************************************************************************************************
*	函 数 名: copy_app(u32 WriteAddr,u16 *pBuffer,u16 NumToWrite)   
*	形    参：WriteAddr : 要写数据的地址
*			  		pBuffer:pBuffer:数据指针
*						NumToWrite:半字节数
*	返 回 值: 无
*********************************************************************************************************
*/
void CopyApp(u32 Writeaddr,u16 *pBuffer,u16 NumToWrite)
{
	int i=0;
	if(Writeaddr<STM32_FLASH_BASE||(Writeaddr>=(STM32_FLASH_BASE+1024*STM32_FLASH_SIZE)))return;//非法地址
	if(NumToWrite == 0 )return;//非法长度
	FLASH_Unlock();						//解锁
	FLASH_ErasePage(Writeaddr);//擦出当前页的数据
	for( i=0;i<NumToWrite;i++) //写所有的数据
	{
		FLASH_ProgramHalfWord(Writeaddr,pBuffer[i]);
		Writeaddr+=2;
	}
	FLASH_Lock();//上锁
}

/*****************************************************************************
函数名称 : JumpToApp
功能描述 : 程序跳转
输入参数 : 无
返回参数 : 无
使用说明 : 无
*****************************************************************************/
void JumpToApp(uint32_t app_address)                  
{
  typedef  void (*pFunction)(void);
	RCC_DeInit(); //关闭外设
  __disable_irq(); //关中断（）
	/*判断栈定地址值是否在0x2000 0000 - 0x 2000 2000之间*/
	if(((* (__IO uint32_t *)app_address) & 0x2FFE0000) == 0x20000000)
	{
		pFunction Jump_To_Application;
		uint32_t JumpAddress;
		
		JumpAddress = *(__IO uint32_t*) (app_address + 4);
		Jump_To_Application = (pFunction) JumpAddress;
		
		__set_MSP(*(__IO uint32_t*) app_address);
		Jump_To_Application();
	}
}
/*****************************************************************************
函数名称 : ByteToHalfWord(u8 *inaddr,u16 *outaddr,u32 inlen)
功能描述 : 字节转换为半字
输入参数 : inaddr 串口接受到的数值
           outaddr 要输出的数值
返回参数 : 无
使用说明 : 无
*****************************************************************************/
void WriteAppBin(u32 appaddr,u8 *appbuf,u32 appsize)
{
	u16 t=0;
	u16 i=0;
	u16 temp=0;
	u32 fwaddr=appaddr;//当前写入的地址
	u8 *dfu=appbuf;
	for(t=0;t<appsize;t+=2)
	{						    
		temp=(u16)dfu[1]<<8;
		temp+=(u16)dfu[0];	  
		dfu+=2;//偏移2个字节
		gIAPBUF[i++]=temp;	    
		if(i==1024)
		{
			i=0;
			CopyApp(fwaddr,gIAPBUF,1024);	
			fwaddr+=2048;//一次写2K
		}
	}
	if(i)CopyApp(fwaddr,gIAPBUF,i);//将最后的一些内容字节写进去.  
}



