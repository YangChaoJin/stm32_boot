
/*
*********************************************************************************************************
*
*	模块名称 : 串口
*	文件名称 : #include "bsp_usart.h"
*	版    本 : V1.0
*	说    明 : 头文件

*********************************************************************************************************
*/  
#include "bsp_usart.h"
#include <stdio.h>
/*
*********************************************************************************************************
*	函 数 名: USART1_Config(uint32_t BaudRate,)
*	功能说明: 电磁阀IO口初始化
*	形    参：BaudRate 波特率
*	返 回 值: 无
*********************************************************************************************************
*/
static void USART1_Config(uint32_t BaudRate)
{
	GPIO_InitTypeDef GPIO_InitStructure;
	USART_InitTypeDef USART_InitStructure;
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_USART1|RCC_APB2Periph_GPIOA, ENABLE);
	/* USART1 GPIO config */
	/* Configure USART1 Tx (PA.09) as alternate function push-pull */
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_9;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF_PP;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_Init(GPIOA, &GPIO_InitStructure);    
	/* Configure USART1 Rx (PA.10) as input floating */
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_10;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN_FLOATING;
	GPIO_Init(GPIOA, &GPIO_InitStructure);
	
	/* USART1 mode config */
	USART_InitStructure.USART_BaudRate = BaudRate;
	USART_InitStructure.USART_WordLength = USART_WordLength_8b;
	USART_InitStructure.USART_StopBits = USART_StopBits_1;
	USART_InitStructure.USART_Parity = USART_Parity_No ;
	USART_InitStructure.USART_HardwareFlowControl = USART_HardwareFlowControl_None;
	USART_InitStructure.USART_Mode = USART_Mode_Rx | USART_Mode_Tx;
	USART_Init(USART1, &USART_InitStructure);
	
	/* 使能串口1接收中断 */
	USART_ITConfig(USART1, USART_IT_RXNE, ENABLE);
	
	USART_Cmd(USART1, ENABLE);
}
/*
*********************************************************************************************************
*	函 数 名: NVIC_UART2Config
*	功能说明: UART2中断配置
*	形    参：NONE
*	返 回 值: NONE
*********************************************************************************************************
*/
static void NVIC_UARTConfig(void)
{
	NVIC_InitTypeDef NVIC_InitStructure; 
	/* Configure the NVIC Preemption Priority Bits */  
  NVIC_PriorityGroupConfig(NVIC_PriorityGroup_1);// 设置中断优先级分组2
	/* Enable the USART2 Interrupt */
	NVIC_InitStructure.NVIC_IRQChannel = USART1_IRQn;	 
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 1;
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0;
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init(&NVIC_InitStructure);

	NVIC_Init(&NVIC_InitStructure);
}

/*
*********************************************************************************************************
*	函 数 名: UARTConfig
*	功能说明: 串口初始化
*	形    参：NONE
*	返 回 值: NONE
*********************************************************************************************************
*/
void UARTConfig(void)
{
	NVIC_UARTConfig();
	USART1_Config(115200);
}

int fputc(int ch, FILE* stream)
{
    while(USART_GetFlagStatus(USART1, USART_FLAG_TC) == RESET);	 
    USART_SendData(USART1, (uint8_t)ch);
    return ch;
}

/***************************************************************************************
 函数  ： debugSendData(u8 *buf,u8 len)
 参数  ：buf发送数据  len是长度
 返回值: 无
 ***************************************************************************************/
void debugSendData(uint8_t *buf,uint8_t len)
{
	uint8_t i=0;
 	for(i=0;i <len;i++)	 //循环发送数据
	{		   
		while(USART_GetFlagStatus(USART1, USART_FLAG_TC) == RESET);	  
		USART_SendData(USART1,buf[i]);
	}	 
}

/*********************************************END OF FILE**********************/
