/*
*********************************************************************************************************
*	                                  
*
*********************************************************************************************************
*/
#ifndef __BSP_FLASH_H_
#define __BSP_FLASH_H_

#include "stm32f10x.h"

void CopyApp(u32 Writeaddr,u16 *pBuffer,u16 NumToWrite);
void JumpToApp(uint32_t app_address);
void WriteAppBin(u32 appaddr,u8 *appbuf,u32 appsize);
#endif  /* __FLASH_IF_H */
